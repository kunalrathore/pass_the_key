import xmltodict

from rest_framework import status
from rest_framework.test import APITestCase

from api.libs.postcodes import PostcodeAPI
from api.management.commands import setupdb
from .models import Listing
from .serializers import ListingSerializer


class APITest(APITestCase):
    """
        APITest : for testing API endpoints.
    """

    def setUp(self):
        cmd = setupdb.Command()
        cmd.handle()

    def test_outcode_list(self):
        """
        test_outcode_list : test /api/outcode/<outcode>/ endpoint
        """
        company_object = Listing.objects.all().first()
        response = self.client.get(
            "/api/outcode/" + company_object.outcode
        )
        outcode_set = Listing.objects.filter(
            outcode=company_object.outcode
        )
        serialized_outcodes = ListingSerializer(outcode_set, many=True)
        for item in serialized_outcodes.data:
            item["id"] = str(item["id"])
            item["listing_id"] = str(item["listing_id"])
        self.assertEqual(
            xmltodict.parse(response.content)["outcode"]["listing"],
            serialized_outcodes.data,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_nearest_postcode(self):
        """
        test_nearest_postcode : test /api/nexus/<postcode>/ endpoint
        """
        response = self.client.get("/api/nexus/M46JA")
        postcodes_list, distance = PostcodeAPI().get_neighbour("M46JA")
        result = []

        for post_code in postcodes_list:
            listings = Listing.objects.filter(zipcode=post_code)
            listings_serializer = ListingSerializer(listings, many=True)
            if listings.count() != 0:
                result.append(listings_serializer.data)

        #convert id and listing_id to str because xml response is in string
        for item in result[0]:
            item["id"] = str(item["id"])
            item["listing_id"] = str(item["listing_id"])
        self.assertEqual(
            xmltodict.parse(response.content)["outcodes"]["outcode"][
                "listing"
            ],
            result[0][0],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
