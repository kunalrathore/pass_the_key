# Instructions

Django application to help franchise acquisition team understand the value of
postcode districts in Greater Manchester.

## Update .env file

Update values in .env


## Commands to Run Project


```bash
cd pass_the_key
```

Update values in .env and dev.py file

```bash
sudo docker-compose build
sudo docker-compose up -d
sudo docker-compose exec web python manage.py migrate --settings=pass_the_key.settings.production
sudo docker-compose exec web python manage.py setupdb --settings=pass_the_key.settings.production
```

## Testing

```bash
sudo docker-compose exec web python manage.py test --settings=pass_the_key.settings.production
```

## Swagger

You can checkout swagger at /api/swagger